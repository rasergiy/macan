#!/usr/bin/python3
# Gather MAC Address table file

import argparse
from maca import MacVendors

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--nmap-file', default=None, 
                            help='Nmap file')
parser.add_argument('-m', '--macaddress-io', default=None, 
                            help='CSV file')
args = parser.parse_args()

mac_vendors = MacVendors()
mac_vendors.load()

nadd = 0
skip = 0
print('Total vendors: %s' % len(mac_vendors))

if args.nmap_file:
    n, a = mac_vendors.import_nmap_file(args.nmap_file)
    nadd+=n
    skip+=a
if args.macaddress_io:
    n, a = mac_vendors.import_macaddress_io(args.macaddress_io)
    nadd+=n
    skip+=a
if nadd:
    print('%s vendors added' % nadd)
if skip:
    print('%s vendors skipped' % skip)
mac_vendors.load()
mac_vendors.close()
