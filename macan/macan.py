#!/usr/bin/python3
# (c) RaSergiy, 2021
# Public MAC check service: https://2ip.ua/ru/services/information-service/mac-find

import re
import os
import csv  
import sqlite3
import argparse

class Mac:

    mac = None          # Mac address
    hexmac = None       # Mac address as hex number
    intmac = None       # Mac address as integer number
    multicast = False   # Multicast/unicast bit
    local = False       # Local/global bit
    device_name = None  # BT Device name 
    logical_name = None # BT Logical device name
    name = None         # Device name
    hexoui = None       # Organisationaly unique ID
    vendor = None       # Vendor name

    def __init__(self, mac, mac_vendors=None, logical_name=None, device_name=None):
        self.mac = mac.upper()
        if not re.match(('[A-F0-9]{2}:'*6)[:-1], self.mac):
            raise BaseException('Wrong mac format: %s' % (mac))
        self.hexmac = mac.replace(':','')
        self.hexoui = self.hexmac[:6]
        b = int(self.hexmac[:2], 16)
        self.intmac = int(self.hexmac, 16)
        self.local = bool(b & 2)
        self.multicast = bool(b & 1)
        self.device_name = device_name
        self.logical_name = logical_name
        if device_name and logical_name:
            if device_name == logical_name:
                self.name = device_name
            else:
                self.name = '%s (%s)' % (device_name, logical_name)
        else:
            self.name = device_name or logical_name
        if mac_vendors and self in mac_vendors:
            self.vendor = mac_vendors[self]

    def __str__(self):
        if self.multicast:
            mu = 'Multicast'
        else:
            mu = 'Unicast'
        if self.local:
            lu = 'Local'
        else:
            lu = 'Universal'
        name = self.name or ''
        vendor = self.vendor or '-'
        return '%s %s %s <%s> %s' % (self.mac, mu, lu, 
                self.vendor or '-', self.name or '')

    def __eq__(self, mac):
        return self.mac==mac.mac

class MacVendors:

    SQL_SCHEME = '''CREATE TABLE macs (id integer NOT NULL,\
            oui TEXT NOT NULL, vendor TEXT NOT NULL);'''
    SQL_INSERT = '''INSERT INTO macs (id, oui, vendor)\
            VALUES  (%s, "%s", "%s")'''

    def __init__(self, dbname='macvendor.db'):
        self.db = dict()
        new_file = not os.path.exists(dbname)
        self.sqlite_connection = sqlite3.connect(dbname)
        self.sqlite_connection.row_factory = sqlite3.Row
        self.cursor = self.sqlite_connection.cursor()
        if new_file:
            self.cursor.execute(self.SQL_SCHEME)

    def load(self):
        query = 'select * from macs'
        result = self.cursor.execute(query)
        rows = self.cursor.fetchall()
        for row in rows:
            self.db[row['oui']] = row['vendor']
    
    def close(self):
        self.sqlite_connection.commit()
        self.cursor.close()

    def import_nmap_file(self, infile='/usr/share/nmap/nmap-mac-prefixes'):
        nadd, skip = 0, 0
        for line in open(infile, 'r').readlines():
            if not line[0]=='#':
                oui = line[:6].upper()
                id = int(oui, 16)
                if oui in self.db:
                    skip += 1
                else:
                    nadd += 1
                    vendor = line[7:].strip()
                    self.cursor.execute(self.SQL_INSERT % (id, oui, vendor))
                    self.db[oui] = vendor
        return nadd, skip

    def import_macaddress_io(self, infile):
        nadd, skip = 0, 0
        with open(infile, encoding = "UTF8") as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for nrow, row in enumerate(reader):
                row = [c.strip() for c in row]
                if nrow==0 and row!=['oui','isPrivate','companyName',
                        'companyAddress','countryCode','assignmentBlockSize',
                        'dateCreated','dateUpdated']:
                    raise BaseException('Invalid csv file', row)
                if nrow>0:
                    mac = row[0]
                    if len(mac) < 8:
                        print('Illegal ouid: %s', mac)
                    oui = mac.replace(':','').upper()[:6]
                    if oui in self.db:
                        skip += 1
                    else:
                        nadd += 1
                        id = int(oui, 16)
                        vendor = row[2].replace('"', '""')
                        self.cursor.execute(self.SQL_INSERT % (id, oui, vendor))
                        self.db[oui] = vendor
        return nadd, skip
    
    def db_close(self):
        self.cursor.close()

    def __contains__(self, mac):
        return mac.hexoui in self.db

    def __getitem__(self, mac):
        return self.db[mac.hexoui]

    def __len__(self):
        return len(self.db)

class MacList(list):

    def __init__(self, mac_vendors):
        super().__init__()
        self._mac_vendors = mac_vendors

    def import_ble_scanner_csv(self, csv_file):
        with open(csv_file, encoding = "UTF8") as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar="'")
            for nrow, row in enumerate(reader):
                row = [c.strip() for c in row]
                if nrow==0 and row!=['', 'BLE Scanner History Log','']:
                    raise BaseException('Invalid csv file', row)
                if nrow==1 and row!=['DeviceName', 'Logical Name', 
                        'MacAddress', 'DeviceAddedTime', 'LastSeenTime']:
                    raise BaseException('Invalid csv file', row)
                if nrow>1:
                    for ncol, col in enumerate(row):
                        devname = row[0]
                        if devname=='N/A':
                            devname = None
                        logname = row[1]
                        if logname=='N/A':
                            logname = None
                        mac = Mac(row[2], mac_vendors=self._mac_vendors,
                            logical_name=logname, device_name=devname)
                        if not mac in self:
                            self.append(mac)

    def import_list(self, macs):
        for mac in macs:
            if not mac in self:
                self.append(Mac(mac, mac_vendors=self._mac_vendors))

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('mac', metavar='mac-address', nargs='*',
                                help='Mac addresses')
    parser.add_argument('-c', '--csv-file', default=None,
                                help='CSV File (BLE Scanner format)')
    parser.add_argument('-n', '--filter-name', default=False, action='store_true',
                                help='Filter named devices')
    parser.add_argument('-N', '--filter-noname', default=False, action='store_true',
                                help='Filter non named devices')
    parser.add_argument('-v', '--filter-vendor', default=False, action='store_true',
                                help='Filter vendor devices')
    parser.add_argument('-V', '--filter-nonvendor', default=False, action='store_true',
                                help='Filter non vendor devices')
    parser.add_argument('-l', '--filter-local', default=False, action='store_true',
                                help='Filter Local devices')
    parser.add_argument('-L', '--filter-nonlocal', default=False, action='store_true',
                                help='Filter non Local devices')
    parser.add_argument('-m', '--filter-multicast', default=False, action='store_true',
                                help='Filter multicast devices')
    parser.add_argument('-M', '--filter-nonmulticast', default=False, action='store_true',
                                help='Filter non multicast devices')
    args = parser.parse_args()

    mac_vendors = MacVendors()
    mac_vendors.load()
    mac_vendors.db_close()
    macs = MacList(mac_vendors)

    if args.csv_file:
        macs.import_ble_scanner_csv(args.csv_file)

    macs.import_list(args.mac)

    def mac_add_filter(mac):
        add = True
        if args.filter_name and not mac.name:
            add = False
        if args.filter_noname and mac.name:
            add = False
        if args.filter_vendor and not mac.vendor:
            add = False
        if args.filter_nonvendor and mac.vendor:
            add = False
        if args.filter_local and not mac.local:
            add = False
        if args.filter_nonlocal and mac.local:
            add = False
        if args.filter_multicast and not mac.multicast:
            add = False
        if args.filter_nonmulticast and mac.multicast:
            add = False
        return add

    macs = list(filter(mac_add_filter, macs))

    if not macs:
        raise BaseException('No macs specified')

    stat = {'total': len(macs),
            'local': 0,
            'multicast': 0,
            'name': 0,
            'vendor': 0 }
    stat.update({i:0 for i in range(48)})

    for n, mac in enumerate(macs, 1):
        for i in range(48):
            stat[i] += bool(mac.intmac & 2**i)
        stat['multicast'] += bool(mac.multicast)
        stat['local'] += bool(mac.local)
        stat['name'] += bool(mac.name)
        stat['vendor'] += bool(mac.vendor)
        print('%s' % (mac))
    
    stat['multicast_pr'] = 100.0*stat['multicast']/len(macs)
    stat['local_pr'] = 100.0*stat['local']/len(macs)
    stat['vendor_pr'] = 100.0*stat['vendor']/len(macs)
    stat['name_pr'] = 100.0*stat['name']/len(macs)

    print('''\
--- SUMMARY ------------------------------------------------------------
Addresses: %(total)s
Multicast addresses: %(multicast)s (%(multicast_pr)0.2f%%)
Local addresses: %(local)s (%(local_pr)0.2f%%)
Named devices: %(name)d (%(name_pr)0.2f%%)
Known vendor devices: %(vendor)d (%(vendor_pr)0.2f%%)
------------------------------------------------------------------------
BIT%%:    8       7       6       5       4       3       2       1
    --------------------------------------------------------------------''' % stat)
    for i in reversed(range(48)):
        s = ('%0.2f%%' % (100.0*stat[i]/len(macs))).rjust(7)
        if not ((i+1) % 8):
            a = ['%d   | %s' % (7-i/8, s)]
        else:
            a.append(s)
        if not (i % 8):
            print(' '.join(a))


